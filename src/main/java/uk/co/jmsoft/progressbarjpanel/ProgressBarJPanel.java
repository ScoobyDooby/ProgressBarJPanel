/*
 * The MIT License
 *
 * Copyright 2017 John Marshall.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package uk.co.jmsoft.progressbarjpanel;

import java.awt.Graphics;
import javax.swing.JPanel;

/**
 * A simple progress bar based on javax.swing.JPanel.
 * Can be imported into the Netbeans GUI builder pallet.
 * @author Scooby
 */
public class ProgressBarJPanel extends JPanel
{

    private long min = 0;
    private long max = 100;
    private long currentPosition = 0;

    public long getCurrent()
    {
        return currentPosition;
    }

    /**
     * Sets the lower bound the progress bar counts between.
     * Should be less than the upper bound.
     * <p>
     * @param min
     */
    public void setMin(long min)
    {
        this.min = min;
    }

    /**
     * Sets the upper bound the progress bar counts between.<br>
     * Should be greater than the lower bound.
     * <p>
     * @param max
     */
    public void setMax(long max)
    {
        this.max = max;
    }

    /**
     * Gets the lower bound the progress bar counts between.
     * <p>
     * @return
     */
    public long getMin()
    {
        return min;
    }

    /**
     * Gets the upper bound the progress bar counts between.
     * <p>
     * @return
     */
    public long getMax()
    {
        return max;
    }

    /**
     * Sets the current position of the progress bar between the upper and lower
     * bounds. <br>Values outside the bounds set the progress bar to its minimum
     * or maximum position.
     * <p>
     * @param current
     */
    public void setCurrentPosition(long current)
    {
        this.currentPosition = current;
        this.repaint();
    }

    @Override
    protected void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        // Work out the fraction first to avoid overflowing the max int value.
        int currentPixels = (int)(this.getWidth() * (1.0 * currentPosition) / max);

        // Paint background color over whole panel
        g.setColor(this.getBackground());
        g.fillRect(0, 0, this.getWidth(), this.getHeight());

        // Paint current progress position
        g.setColor(this.getForeground());
        g.fillRect(0, 0, currentPixels, this.getHeight());

    }

}
